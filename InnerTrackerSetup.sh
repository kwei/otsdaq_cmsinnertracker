echo # This script is intended to be sourced.

sh -c "[ `ps $$ | grep bash | wc -l` -gt 0 ] || { echo 'Please switch to the bash shell before running the otsdaq-demo.'; exit; }" || exit

echo -e "setup [275]  \t ======================================================"
echo -e "setup [275]  \t Initially your products path was PRODUCTS=${PRODUCTS}"

unsetup_all

export PRODUCTS=/cvmfs/fermilab.opensciencegrid.org/products/artdaq/
export OTSDAQ_HOME=$PWD
#export OTSDAQ_CMSOUTERTRACKER_DIR=${OTSDAQ_HOME}/srcs/otsdaq_cmsoutertracker/
#export OTSDAQ_CMSINNERTRACKER_DIR=${OTSDAQ_HOME}/srcs/otsdaq_cmsinnertracker/
source ${PRODUCTS}/setup
        
export CompileForHerd=true
export CompileForShep=true

export CompileWithEUDAQ=false

export EmulationFlag=true
export DeepEmulationFlag=true

setup mrb
setup git
#source ${OTSDAQ_HOME}/localProducts_otsdaq_v2_04_03_indev_s96_e19_prof/setup
source ${OTSDAQ_HOME}/localProducts_otsdaq__s96_e19_prof/setup
source mrbSetEnv

echo -e "setup [275]  \t Now your products path is PRODUCTS=${PRODUCTS}"
echo

# Setup environment when building with MRB (As there's no setupARTDAQOTS file)

export CETPKG_INSTALL=${PRODUCTS}
export CETPKG_J=64

export OTS_MAIN_PORT=2016

        export USER_DATA="${OTSDAQ_HOME}/srcs/otsdaq_cmsinnertracker/DataDefault"
        export ARTDAQ_DATABASE_URI="filesystemdb://${OTSDAQ_HOME}/srcs/otsdaq_cmsinnertracker/databases/filesystemdb/default_db"
        export OTSDAQ_DATA="/home/kwei726/data"
        export PH2ACF_ROOT=${OTSDAQ_HOME}/srcs/otsdaq_cmsinnertracker/otsdaq-cmsinnertracker/Ph2_ACF
        export ACFSUPERVISOR_ROOT=${OTSDAQ_HOME}/srcs/otsdaq_cmsinnertracker/otsdaq-cmsinnertracker/ACFSupervisor
        		
        echo -e "setup [275]  \t Now your user data path is USER_DATA \t\t = ${USER_DATA}"
        echo -e "setup [275]  \t Now your database path is ARTDAQ_DATABASE_URI \t = ${ARTDAQ_DATABASE_URI}"
        echo -e "setup [275]  \t Now your output data path is OTSDAQ_DATA \t = ${OTSDAQ_DATA}"
echo

        alias rawEventDump="art -c /home/modtest/Programming/otsdaq/srcs/otsdaq/artdaq-ots/ArtModules/fcl/rawEventDump.fcl"
        alias kx='StartOTS.sh -k'
       
        echo
        echo -e "setup [275]  \t Now use 'StartOTS.sh --wiz' to configure otsdaq"
        echo -e "setup [275]  \t  	Then use 'StartOTS.sh' to start otsdaq"
        echo -e "setup [275]  \t  	Or use 'StartOTS.sh --help' for more options"
        echo
        echo -e "setup [275]  \t     use 'kx' to kill otsdaq processes"
echo
  
setup ninja v1_8_2
alias makeninja='pushd $MRB_BUILDDIR; ninja; popd'
#mrb z
#mrb b --generator ninja
