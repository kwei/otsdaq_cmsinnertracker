include_directories($ENV{OTSDAQ_CMSOUTERTRACKER_DIR}/uhal/uhal_2_4_2/cactuscore/uhal/log/include)

cet_set_compiler_flags(
 EXTRA_FLAGS -D MAX_NUM_ARGS=32
 )

cet_make(LIBRARY_NAME cactus_uhal_log 
        LIBRARIES
	${Boost_SYSTEM_LIBRARY}
	${Boost_THREAD_LIBRARY}
	pthread
        )

install_headers()
install_source()
