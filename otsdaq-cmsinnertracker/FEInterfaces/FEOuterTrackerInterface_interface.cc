#include "otsdaq-cmsoutertracker/UserConfigurationDataFormats/MPAPixelConfiguration.h"
#include "otsdaq-cmsoutertracker/UserConfigurationDataFormats/MPAPeripheryConfiguration.h"


#include "otsdaq-core/ConfigurationInterface/DACStream.h"
#include "otsdaq-core/MessageFacility/MessageFacility.h"
#include "otsdaq-core/Macros/CoutMacros.h"
#include "otsdaq-core/Macros/InterfacePluginMacros.h"

#include <iostream>

#include <bitset>

#include "../UserConfigurationDataFormats/FEOuterTrackerInterfaceConfiguration.h"
#include "FEOuterTrackerInterface.h"

using namespace ots;


//========================================================================================================================
FEOuterTrackerInterface::FEOuterTrackerInterface(std::string interfaceID, std::string interfaceType, const FEInterfaceConfigurationBase* configuration)
: FEVInterface              (interfaceID, interfaceType, configuration)
, UDPDataStreamer           (static_cast<const FEWROuterTrackerInterfaceConfiguration*>(configuration)->getIPAddress(interfaceID)
		, static_cast<const FEWROuterTrackerInterfaceConfiguration*>(configuration)->getPort(interfaceID)
		, static_cast<const FEWROuterTrackerInterfaceConfiguration*>(configuration)->getStreamToIPAddress(interfaceID)
		, static_cast<const FEWROuterTrackerInterfaceConfiguration*>(configuration)->getStreamToPort(interfaceID))
, theConfiguration_         (static_cast<const FEWROuterTrackerInterfaceConfiguration*>(configuration))
, thePixelConfiguration_    (0)
, thePeripheryConfiguration_(0)
, uasic_                    (theConfiguration_->getConnectionFile(interfaceID), theConfiguration_->getDeviceID(interfaceID))
, mapsa_                    (uasic_)
, mpa_                      (uasic_.getHardware(), 0) //this is temporary
{
	std::cout << __PRETTY_FUNCTION__ << "-" << theConfiguration_->getDeviceID(interfaceID) << std::endl;
	//turn on the mapsa (order matters here)
	mapsa_.VDDPST_on();
	sleepMS(500);
	mapsa_.DVDD_on();
	sleepMS(500);
	mapsa_.AVDD_on();
	sleepMS(500);
	mapsa_.PVDD_on();
//
//	uasic_.getHardware()->dispatch();//FIXME This should be removed since there is a dispatch in each of the previous mapsa_ commands!!!!!

	//todo need to refactor all of the uasic_.getHardware()->foo()'s to just call
	//mapsa_.hw_->foo()
}

//========================================================================================================================
FEOuterTrackerInterface::~FEOuterTrackerInterface (void)
{

}

//========================================================================================================================
void FEOuterTrackerInterface::configure(void)
{
	//need to rewrite this method to use ots configuration files
	writePixelAndPeripheryConfiguration();

	//read out MPA configurations (only config_1 is used?)
	for(int i = 1; i <= 6; ++i)
	{
		auto read = uasic_.getHardware()->getNode("Configuration")
    		  .getNode("Memory_DataConf").getNode("MPA" + std::to_string(i)).getNode("config_1").read();
		uasic_.getHardware()->dispatch();

		//24 pairs of pixels written after periphery (25 blocks total)
		auto blockRead = uasic_.getHardware()->getNode("Configuration")
    		  .getNode("Memory_DataConf").getNode("MPA" + std::to_string(i)).getNode("config_1").readBlock(25);
		uasic_.getHardware()->dispatch();

		int j = 0;
		for(const auto& item : blockRead)
		{
			if(j == 0)
			{
				//this is the periphery
				std::cout << "periphery: " << std::bitset<32>(item) << std::endl;
			}
			else
			{
				//this is a pixel
				std::cout << "pixel " << j << "-" << j+1 << ": " << std::bitset<20>(item) << std::endl;
				std::cout << "Dec: " << item << std::endl;
			}
			++j;
		}
	}

}

//========================================================================================================================
void FEOuterTrackerInterface::halt (void)
{
	//not sure how this is differentiated from pause
}

//========================================================================================================================
void FEOuterTrackerInterface::pause (void)
{
	//clear readout and shutter settings
} 

//========================================================================================================================
void FEOuterTrackerInterface::resume (void)
{
	//rewrite shutter settings
}

//========================================================================================================================
void FEOuterTrackerInterface::start (std::string runNumber)
{
	//basic implementation - write shutter settings and readout data.

	uint32_t shutterNum, shutterDelay, shutterLength, shutterDistance, shutterDuration,
	calibration;

	//using hardcoded defaults here.
	//should either make a shutter configuration interface or
	//have them as part of the FE Interface
	shutterNum = 500;
	shutterDuration = 4;
	shutterDelay = 0;
	shutterDistance = 0xFF;
	shutterLength = 0xFFF;

	uasic_.getHardware()->getNode("Control").getNode("MPA_clock_enable").write(0x1);
	uasic_.getHardware()->dispatch();

	uasic_.getHardware()->getNode("Shutter").getNode("Strobe")
    		.getNode("number").write(shutterNum);
	uasic_.getHardware()->getNode("Shutter").getNode("Strobe")
    		.getNode("length").write(shutterLength);
	uasic_.getHardware()->getNode("Shutter").getNode("Strobe")
    		.getNode("distance").write(shutterDistance);
	uasic_.getHardware()->getNode("Shutter").getNode("Strobe")
    		.getNode("delay").write(shutterDelay);


	uasic_.getHardware()->getNode("Control").getNode("beam_on").write(0x1);

	uasic_.getHardware()->getNode("Control").getNode("calibration").write(0x1);

	uasic_.getHardware()->dispatch();


	//take data
	///////////
	std::cout << "\n\n\n\n\n\n\nSTARTING\n\n\n\n\n\n\n";
	uasic_.getHardware()->getNode("Shutter").getNode("time").write(shutterDuration);
	uasic_.getHardware()->getNode("Control").getNode("testbeam_mode").write(0x0);
	uasic_.getHardware()->getNode("Control").getNode("readout").write(1);
	uasic_.getHardware()->dispatch();

}

//========================================================================================================================
bool FEOuterTrackerInterface::running(void)
{
	//concatenate 216 * 32bit words (6912 bits)
	std::bitset<6912> memory1D;

	using memRow = std::bitset<72>;//96 * 72 rows of memory (read as 32 * 216) = 6912 bits
	std::vector<memRow> memRowArray;

//	for(int x = 0; x < 10; ++x)
//	{
		for(int i = 1; i <= 6; ++i)
		{
			//initialize headers
			uasic_.getHardware()->getNode("Readout").getNode("Header")
			.getNode("MPA" + std::to_string(i)).write(0xFFFFFFF0+i);
			uasic_.getHardware()->dispatch();
		}

		uasic_.getHardware()->getNode("Control").getNode("Sequencer")
    		  .getNode("datataking_continuous").write(0x1);

		uasic_.getHardware()->getNode("Control").getNode("Sequencer")
    		  .getNode("buffers_index").write(1);//Only buffer 1 will be filled
		uasic_.getHardware()->dispatch();


		std::cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
		for(int i = 1; i <= 6; ++i)
		{
			for(int j = 1; j < 5; ++j) //There are 4 buffers but only buffer 1 has data because I said so before (3 lines above)
			{
				auto counterData = uasic_.getHardware()->getNode("Readout").getNode("Counter")
			  .getNode("MPA"+std::to_string(i)).getNode("buffer_" + std::to_string(j)).readBlock(25);

				auto memoryData = uasic_.getHardware()->getNode("Readout").getNode("Memory")
			  .getNode("MPA"+std::to_string(i)).getNode("buffer_" + std::to_string(j)).readBlock(216);

				uasic_.getHardware()->dispatch();

				//std::cout << "Counter Data: \n\n";
				int row = 0;
				// for(const auto& data : counterData) {
				//   std::cout << "Counter data: " << std::bitset<0x19>(data) << std::endl;
				// }

				std::size_t index = 0;
				std::string reversedData;
				for(const auto& data : memoryData)
				{
					for(size_t i = 0; i < 32; ++i)
					{
						//set bits of 1d memory block to bits of 32 bit data block from readout
						memory1D[index + i] = static_cast< std::bitset<32> >(data)[i];
					}
					index += 32;
					if(data != 0)
					{
						//for debugging row by row
//						std::cout << "Row " << row << " of memory contains data:\n"
//								<< std::bitset<0x20>(data) << std::endl;
					}
					++row;
				}
				TransmitterSocket::send(streamToSocket_, memory1D.to_string());

				index = 0;
				memRow temp;
				bool display = false;
				for(size_t i = 0; i < 96; ++i) {
					for(size_t j = 0; j < 72; ++j) {
						temp[j] = memory1D[index +j];
						if(temp[j] != 0) display = true;
					}
					index += 72;
					//TODO at this point, the mem block is in the reverse order, i think??
					//It's reversed further down to fix this
					memRowArray.push_back(temp);
				}
				if(display) {
					//displays formatted memory.
//					std::cout << "Formatted Rows: \n";
//					for(const auto& row : memRowArray) {
//						std::cout << row << std::endl;
//					}
				}
			}
		}
		sleepMS(2500);
//	}

	std::vector< std::bitset<48> > hitVector;
	std::bitset<48> hitRow;
	//for each row in the grid, the last 48 bits are the hits
	//make a new vector that contains the hits for each pixel
	for(const auto& row : memRowArray)
	{
		for(size_t i = 24; i < 72; ++i)
		{
			hitRow[24 - i] = row[i];
		}
		hitVector.push_back(hitRow);
	}

	std::map<int, int> hitCounts;
	for(size_t i = 0; i < 48; ++i) {
		hitCounts[i] = 0;
	}

	std::cout << "\n\n\n\n\n\n checking hitmap \n\n\n\n\n\n";
	for(const auto& hitBlock : hitVector) {
		std::string hitString = hitBlock.to_string();
		std::reverse(hitString.begin(), hitString.end());
		std::bitset<48> reversedData (hitString);
		for(size_t index = 0; index < 48; ++index) {
			if(reversedData[index]) {
				hitCounts[index] += 1;
			}
		}
	}

	//read out hit totals
	for(size_t i = 0; i < 48; ++i) {
		std::cout << "Pixel " << getPixelCoordinate(i) << " has " << hitCounts[i] << " hits.\n";
	}
	return WorkLoop::continueWorkLoop_;//otherwise it stops!!!!!
}

//========================================================================================================================
void FEOuterTrackerInterface::stop (void)
{
	//power off. order matters here.
	mapsa_.PVDD_off();

	mapsa_.AVDD_off();

	mapsa_.DVDD_off();

	mapsa_.VDDPST_off();

	uasic_.getHardware()->dispatch();
}


//========================================================================================================================
int FEOuterTrackerInterface::getPixelCoordinate(size_t pixel) {
	//returns the actual coordinate of a pixel given the pixel number from the
	//synchronous readout. returns -1 if the provided pixel index is invalid

	if( pixel > 48 ) return -1;
	if( pixel >= 16 && pixel < 32) return pixel;
	return 47 - pixel;
}

//========================================================================================================================
bool sortPixels(MPAPixelConfiguration::Pixel pixel1, MPAPixelConfiguration::Pixel pixel2)
{
	//used as the parameter to std::sort to sort the pixels in our pixel map
	return pixel1.Number_ < pixel2.Number_;
}

//========================================================================================================================
void FEOuterTrackerInterface::writePixelAndPeripheryConfiguration(void)
{ 
	//get a map of uint32_t (mpa numer) and Pixel struct which has all of the
	//parameters we need to configure the pixels.
	thePixelConfiguration_     = theConfigurationManager_->getConfiguration<MPAPixelConfiguration>("MPAPixelConfiguration");
	thePeripheryConfiguration_ = theConfigurationManager_->getConfiguration<MPAPeripheryConfiguration>("MPAPeripheryConfiguration");
	MPAPixelConfiguration::MPANumberToPixelMap pixelMap = thePixelConfiguration_->getPixelMap();
	//do the same for the periphery.
	MPAPeripheryConfiguration::MPANumberToPeripheryMap peripheryMap = thePeripheryConfiguration_->getPeripheryMap();

	//this will hold the entire configuraiton for an MPA to be
	//written with uhal::blockWrite()
	std::vector<uint32_t> configurationWords;

	//at the end there should be 6 periphery configuration words
	//and 24 * 6 pixel configuration words, so 150 elements total.
	//!!!(per each loop, there will be 25)!!!

	for(uint32_t i = 1; i <= 6; ++i) {

		//handle periphery same way as pixels,
		//then push the 32 bit word to the vector.
		MPAPeripheryConfiguration::Periphery periphery = peripheryMap[i];

		std::bitset<2> OM (periphery.OM_);
		std::bitset<2> RT (periphery.RT_);
		std::bitset<4> SCW (periphery.SCW_);
		std::bitset<4> SH2 (periphery.SH2_);
		std::bitset<4> SH1 (periphery.SH1_);
		std::bitset<8> CALDAC (periphery.CALDAC_);
		std::bitset<8> THDAC  (periphery.THDAC_);

		std::string peripheryConfigString =
				THDAC.to_string() + CALDAC.to_string() + SH1.to_string() +
				SH2.to_string() + SCW.to_string() + RT.to_string() +
				OM.to_string();

		std::bitset<32> peripheryConfig ( peripheryConfigString );
		configurationWords.push_back( static_cast<uint32_t>(peripheryConfig.to_ulong()) );

		//first, sort the pixel vector by number in case they were configured
		//out of order using our custom comparison function.
		//this is so we can go through each one and assume that the pixel
		//is in the order we're expecting.
		std::sort(pixelMap[i].begin(), pixelMap[i].end(), sortPixels);

		//then, for each pixel in the vector of pixels for MPA[i]
		for(const auto& pixel : pixelMap[i]) {
			//read in the configuration parameters
			//to a bistet of an appropriate size. finally, concatenate them to make a 32 bit
			//configuration word making sure to note which row the pixel is in.

			std::bitset<1> PML (pixel.PML_);
			std::bitset<1> ARL (pixel.ARL_);
			std::bitset<5> TRIMDACL (pixel.TRIMDACL_);
			std::bitset<1> CEL (pixel.CEL_);
			std::bitset<2> CW  (pixel.CW_);
			std::bitset<1> PMR (pixel.PMR_);
			std::bitset<1> ARR (pixel.ARR_);
			std::bitset<5> TRIMDACR (pixel.TRIMDACR_);
			std::bitset<1> CER (pixel.CER_);
			std::bitset<1> SP  (pixel.SP_);
			std::bitset<1> SR  (pixel.SR_);

			std::string pixelConfigString;
			//if row 1 or 3, the order is (from low to high bits) PML, ARL, TRIMDACL, CEL, CW, PMR, ARR, TRIMDACR,
			//CER, SP, and SR as above
			if(pixel.Number_ < 8 || pixel.Number_ >= 16) {
				//construct the pixel config as a binary string from the bitsets.
				pixelConfigString =
						SR.to_string() + SP.to_string() + CER.to_string() + TRIMDACR.to_string()
						+ ARR.to_string() + PMR.to_string() + CW.to_string() + CEL.to_string()
						+ TRIMDACL.to_string() + ARL.to_string() + PML.to_string();
			}
			//if row 2, the order is different (from low to high):
			//PMR, ARR, TRIMDACR, CER, SP, SR, PML, ARL, TRIMDACL, CEL, CW
			else {
				//construct the pixel config as a binary string from the bitsets.
				pixelConfigString =
						CW.to_string() + CEL.to_string() + TRIMDACL.to_string() +
						ARL.to_string() + PML.to_string() + SR.to_string() +
						SP.to_string() + CER.to_string() + TRIMDACR.to_string() +
						ARR.to_string() + PMR.to_string();
			}
			//convert the config string to a bitset (32 bit since we need to convert to
			//a uint32_t. then push it to the vector.
			std::bitset<32> pixelConfig (pixelConfigString);
			configurationWords.push_back( static_cast<uint32_t>(pixelConfig.to_ulong()) );

		}

		std::cout << "num words: " << configurationWords.size() << std::endl;
		//std::cout << "Inside vector:\n";
		//don't need this since we print it out after reading, but can be used for
		//debugging
//		for(const auto& word: configurationWords) {
//			std::cout << std::bitset<32>(word) << std::endl;
//		}


		//write the configuration
		//spi wait -- can refactor this since the mapsa class
		//has this method already implemented
		auto busy = uasic_.getHardware()->getNode("Configuration").getNode("busy").read();
		uasic_.getHardware()->dispatch();
		while(busy) {
			sleepMS(10);
			busy = uasic_.getHardware()->getNode("Configuration").getNode("busy").read();
			uasic_.getHardware()->dispatch();
		}
		//upload configuration to MPA i
		uasic_.getHardware()->getNode("Configuration")
    		  .getNode("Memory_DataConf").getNode("MPA" + std::to_string(i)).getNode("config_1").writeBlock(configurationWords);
		uasic_.getHardware()->dispatch();

		//Actually write the configuration to MPA i (If you don't do this, the configuration will just go back
		//to how it was before)
		uasic_.getHardware()->getNode("Configuration").getNode("num_MPA").write(i);
		uasic_.getHardware()->getNode("Configuration").getNode("mode").write(0x5);
		uasic_.getHardware()->dispatch();

		configurationWords.clear();
	}
}

DEFINE_OTS_INTERFACE(FEOuterTrackerInterface)
