#include "otsdaq/MessageFacility/MessageFacility.h"
#include "otsdaq/Macros/CoutMacros.h"
#include "otsdaq/Macros/InterfacePluginMacros.h"
#include "otsdaq-cmsoutertracker/FEInterfaces/FEMiddlewareInterface.h"
#include "otsdaq-cmsoutertracker/Ph2_ACF/Utils/easylogging++.h"
#include <iostream>

using namespace ots;
INITIALIZE_EASYLOGGINGPP

//========================================================================================================================
FEMiddlewareInterface::FEMiddlewareInterface(const std::string &interfaceUID, const ConfigurationTree &theXDAQContextConfigTree, const std::string &configurationPath)
	: FEVInterface(interfaceUID, theXDAQContextConfigTree, configurationPath)
	, TCPClient(
				theXDAQContextConfigTree_.getNode(configurationPath).getNode("InterfaceIPAddress").getValue<std::string>(),
				theXDAQContextConfigTree_.getNode(configurationPath).getNode("InterfacePort").getValue<unsigned int>())

	, calibrationName_  (theXDAQContextConfigTree_.getNode(configurationPath).getNode("CalibrationName").getValue<std::string>())
	, configurationDir_ (theXDAQContextConfigTree_.getNode(configurationPath).getNode("ConfigurationDir").getValue<std::string>())
	, configurationName_(theXDAQContextConfigTree_.getNode(configurationPath).getNode("ConfigurationName").getValue<std::string>())
{
	
	FEVInterface::registerFEMacroFunction(
		"Calibration",  // feMacroName
		static_cast<FEVInterface::frontEndMacroFunction_t>(
			&FEMiddlewareInterface::calibration),  // feMacroFunction
		std::vector<std::string>{"CalibrationType"}, //calibrationandpedenoise"},             // namesOfInputArgs
		std::vector<std::string>{},    // namesOfOutputArgs
		1);          		// requiredUserPermissions
		
		
} //end constructor

//========================================================================================================================
FEMiddlewareInterface::~FEMiddlewareInterface(void)
{
} //end destructor

//========================================================================================================================
void FEMiddlewareInterface::configure(void)
{
	__FE_COUT__ << "Configure" << std::endl;
	if (!TCPClient::connect(30,1000)) //timeout after 30 seconds
	{
		__FE_COUT_ERR__ << "ERROR CAN'T CONNECT TO SERVER!" << std::endl;
		abort();
	}
	std::string configurationFilePath = configurationDir_ + "/" + configurationName_;
	std::string readBuffer = TCPClient::sendAndReceivePacket("Configure,Calibration:" + calibrationName_ + ",ConfigurationFile:" + configurationFilePath);
	__FE_COUT__ << "Message received: " << readBuffer << std::endl;
}

//========================================================================================================================
void FEMiddlewareInterface::halt(void)
{
	__FE_COUT__ << std::endl;
	std::string readBuffer;
	readBuffer = TCPClient::sendAndReceivePacket("Halt");
	__FE_COUT__ << "Message received: " << readBuffer << std::endl;
}

//========================================================================================================================
void FEMiddlewareInterface::pause(void)
{
	__FE_COUT__ << std::endl;
	std::string readBuffer;
	readBuffer = TCPClient::sendAndReceivePacket("Pause");
	__FE_COUT__ << "Message received: " << readBuffer << std::endl;
}

//========================================================================================================================
void FEMiddlewareInterface::resume(void)
{
	__FE_COUT__ << std::endl;
	std::string readBuffer;
	readBuffer = TCPClient::sendAndReceivePacket("Resume");
	__FE_COUT__ << "Message received: " << readBuffer << std::endl;
}

//========================================================================================================================
void FEMiddlewareInterface::start(std::string runNumber)
{
	__FE_COUT__ << std::endl;
	auto readBuffer = TCPClient::sendAndReceivePacket("Start:{RunNumber:" + runNumber + "}");
	__FE_COUT__ << "Message received: " << readBuffer << std::endl;
}

//========================================================================================================================
//The running state is a thread
bool FEMiddlewareInterface::running(void)
{
	__FE_COUT__ << std::endl;
	std::string readBuffer;
	readBuffer = TCPClient::sendAndReceivePacket("Status?");
	__FE_COUT__ << readBuffer << std::endl;
	if(readBuffer.compare("Done") == 0)
		WorkLoop::continueWorkLoop_ = false;

	return WorkLoop::continueWorkLoop_; //otherwise it stops!!!!!
}

//========================================================================================================================
void FEMiddlewareInterface::stop(void)
{
	__FE_COUT__ << std::endl;
	std::string readBuffer;
	readBuffer = TCPClient::sendAndReceivePacket("Stop");
	//__FE_COUT__ << "Message received: " << readBuffer << std::endl;
}

void FEMiddlewareInterface::calibration(__ARGS__)
{
	__FE_COUT__ << "# of input args = " << argsIn.size() << __E__;
	__FE_COUT__ << "# of output args = " << argsOut.size() << __E__;
	for (auto& argIn : argsIn)
		__FE_COUT__ << argIn.first << ": " << argIn.second << __E__;
		
	std::string calibrationType = __GET_ARG_IN__("CalibrationType",std::string);
	if(calibrationType == "calibrationandpedenoise")
	{
		__FE_COUT__ << "Doing " << calibrationType << __E__;
		//halt();
		calibrationName_ = calibrationType;
		std::string configurationFilePath = configurationDir_ + "/" + configurationName_;
		std::string readBuffer;// = TCPClient::sendAndReceivePacket("Configure,Calibration:" + calibrationName_ + ",ConfigurationFile:" + configurationFilePath);
		__FE_COUT__ << "Message received: " << readBuffer << std::endl;
		//configure();
		//start();
		//---running();
		//stop();
		__FE_COUT__ << "Done with " << calibrationType << __E__;
	}
	else
	{
		__FE_SS__ << "Invalid calibration type " << calibrationType << __E__;
		__FE_SS_THROW__;
	}
		
} //end calibration()

DEFINE_OTS_INTERFACE(FEMiddlewareInterface)
