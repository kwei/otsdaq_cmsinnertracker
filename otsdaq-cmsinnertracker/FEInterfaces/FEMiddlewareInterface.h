#ifndef _ots_FEMiddlewareInterface_h_
#define _ots_FEMiddlewareInterface_h_

#include "otsdaq/FECore/FEVInterface.h"
#include "otsdaq-cmsinnertracker/Ph2_ACF/NetworkUtils/TCPClient.h"

#include <string>
#include <fstream>

namespace ots
{

class FEMiddlewareInterface : public FEVInterface, public TCPClient
{
public:

public:
	FEMiddlewareInterface(const std::string& interfaceUID, const ConfigurationTree& theXDAQContextConfigTree, const std::string& configurationPath);
	virtual ~FEMiddlewareInterface(void);

	void configure        (void);
	void halt             (void);
	void pause            (void);
	void resume           (void);
	void start            (std::string runNumber) override;
	void stop             (void);
	void calibration      (__ARGS__);

	bool running          (void);

	void universalRead	  (char* address, char* readValue)  override  {;}
	void universalWrite	  (char* address, char* writeValue) override {;}

private:
	std::string calibrationName_;
	std::string configurationDir_;
	std::string configurationName_;

};



}

#endif
