# CACTUS
export CACTUSLIB=/opt/cactus/lib
export CACTUSINCLUDE=/opt/cactus/include

# BOOST
export BOOST_LIB=/opt/cactus/lib
export BOOST_INCLUDE=/opt/cactus/include/boost

#ROOT
. /data/ups/setup
#setup gcc v6_4_0
setup xdaq v14_4_0b -q e15:prof
#setup root v6_12_04e -q e15:prof

source /opt/local/root/bin/thisroot.sh
#source /usr/local/bin/thisroot.sh
#export ROOTLIB=/usr/lib64/root
#export ROOTSYS=/usr/local/lib/root

#ZMQ
export ZMQ_HEADER_PATH=/usr/include/zmq.hpp

#XERCESC
export XERCESC_ROOT=/data/ups/xerces_c/v3_1_4b/Linux64bit+3.10-2.17-e15-prof/

#export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig:$PKG_CONFIG_PATH
#export XDAQ_ROOT=/data/ups/xdaq/v14_4_0b/Linux64bit+3.10-2.17-e15-prof
export XDAQ_VERSION=""
export XDAQ_DOCUMENT_ROOT=$XDAQ_ROOT/$XDAQ_VERSION/htdocs
export ACFSUPERVISOR_ROOT=$(pwd)
#export PH2ACF_ROOT=/home/xtaldaq/Ph2_ACF
export PH2ACF_ROOT=../Ph2_ACF
export LD_LIBRARY_PATH=$XDAQ_ROOT/$XDAQ_VERSION/lib:$CACTUSLIB:$BOOST_LIB:$PH2ACF_ROOT/lib:$XERCESC_ROOT/lib:$ROOTLIB:$LD_LIBRARY_PATH

#for rcms
alias test="$XDAQ_ROOT/bin/xdaq.exe -p 2021 -c /home/modtest/Programming/otsdaq/srcs/otsdaq_cmsoutertracker/otsdaq-cmsoutertracker/ACFSupervisor/xml/ACFSupervisor.xml"
#alias test="/opt/xdaq/bin/xdaq.exe -p 50000 -c ~/ACFSupervisor/xml/ACFSupervisor.xml"
