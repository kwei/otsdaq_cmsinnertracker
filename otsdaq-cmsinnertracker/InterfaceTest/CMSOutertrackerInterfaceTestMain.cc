#include "otsdaq-core/ConfigurationInterface/ConfigurationManager.h"
#include "otsdaq-core/ConfigurationDataFormats/ConfigurationGroupKey.h"
#include "otsdaq-core/ConfigurationInterface/ConfigurationInterface.h"
//#include "otsdaq-core/ConfigurationPluginDataFormats/Configurations.h"
//#include "otsdaq-core/ConfigurationPluginDataFormats/FEConfiguration.h"

#include "otsdaq-cmsoutertracker/FEInterfaces/FECBCInterface_interface.cc"

#include <iostream>
#include <fstream>

using namespace ots;

int main(int argc, char **argv)
{
	//Variables
	const int          supervisorInstance_    = 1;
	const unsigned int configurationKeyValue_ = 63;

	ConfigurationManager* theConfigurationManager_ = new ConfigurationManager();

	std::string  XDAQContextConfigurationName_ = "XDAQContextConfiguration";
	std::string  supervisorContextUID_         = "OTEastContext";
	std::string  supervisorApplicationUID_     = "FESupervisorOTEast";
	std::string  interfaceUID_                 = "OTEast";
	std::string  supervisorConfigurationPath_  = "/" + supervisorContextUID_ + "/LinkToApplicationConfiguration/" + supervisorApplicationUID_ + "/LinkToSupervisorConfiguration";
	const ConfigurationTree theXDAQContextConfigTree_ = theConfigurationManager_->getNode(XDAQContextConfigurationName_);

	std::string configurationGroupName = "StripConfigurationGroup";
	std::pair<std::string , ConfigurationGroupKey> theGroup(configurationGroupName, ConfigurationGroupKey(configurationKeyValue_));

	////////////////////////////////////////////////////////////////
	//INSERTED GLOBALLY IN THE CODE
	////////////////////////////////////////////////////////////////
	//
	//  ConfigurationManager*   theConfigurationManager_ = new ConfigurationManager;
	//  FEWInterfacesManager    theFEWInterfacesManager_(theConfigurationManager_, supervisorInstance_);
	//
	//  theConfigurationManager_->setupFEWSupervisorConfiguration(theConfigurationKey_,supervisorInstance_);
	//  theFEWInterfacesManager_.configure();
	//
	////////////////////////////////////////////////////////////////
	//Getting just the informations about the FEWInterface
	////////////////////////////////////////////////////////////////

	theConfigurationManager_->loadConfigurationGroup(theGroup.first, theGroup.second, true);
	FECBCInterface* theInterface_ = new FECBCInterface(
			interfaceUID_,
			theXDAQContextConfigTree_,
			supervisorConfigurationPath_ + "/LinkToFEInterfaceConfiguration/" + interfaceUID_ + "/LinkToFETypeConfiguration");
	std::cout << "Done with new" << std::endl;

	// Test interface class methods here //
	theInterface_->configure();
	theInterface_->start(std::string(argv[1]));
	unsigned int second = 1000;//x1ms
	unsigned int time = 60*60*second;
	unsigned int counter=0;
	while(counter++<time)
		{
		theInterface_->running();//There is a 1ms sleep inside the running
		//std::cout << counter << std::endl;
		}
	theInterface_->stop();

	return 0;
}
