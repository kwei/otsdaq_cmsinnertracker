#include "otsdaq-cmsinnertracker/DataProcessorPlugins/DQMHistoMakerConsumer.h"
#include "otsdaq/Macros/MessageTools.h"
#include "otsdaq/MessageFacility/MessageFacility.h"
#include "otsdaq/Macros/CoutMacros.h"
#include "otsdaq/Macros/ProcessorPluginMacros.h"
#include <chrono>
#include <thread>
#include <TDirectory.h>
#include <TFile.h>
#include <TH1F.h>
#include <TRandom.h>
#include <TCanvas.h>

using namespace ots;

//========================================================================================================================
DQMHistoMakerConsumer::DQMHistoMakerConsumer(std::string supervisorApplicationUID, std::string bufferUID, std::string processorUID, const ConfigurationTree& theXDAQContextConfigTree, const std::string& configurationPath)
: WorkLoop             (processorUID)
, DQMHistosConsumerBase(supervisorApplicationUID, bufferUID, processorUID, LowConsumerPriority)
, Configurable         (theXDAQContextConfigTree, configurationPath)
, saveFile_            (theXDAQContextConfigTree.getNode(configurationPath).getNode("SaveFile").getValue<bool>())
, filePath_            (theXDAQContextConfigTree.getNode(configurationPath).getNode("FilePath").getValue<std::string>())
, radixFileName_       (theXDAQContextConfigTree.getNode(configurationPath).getNode("RadixFileName").getValue<std::string>())

{
}

//========================================================================================================================
DQMHistoMakerConsumer::~DQMHistoMakerConsumer(void)
{
	DQMHistosBase::closeFile();
}
//========================================================================================================================
void DQMHistoMakerConsumer::startProcessingData(std::string runNumber)
{
        random_ = new TRandom() ;
	DQMHistosBase::openFile(filePath_ + "/otsdaq_" + runNumber + ".root");
	TDirectory * MainDir           = DQMHistosBase::theFile_->mkdir("MainDir", "MainDir");        // <---
	MainDir->cd();
	hGaussA_ = new TH1F("GaussianMainA", "Gaussian mainA", 100, 0, 100);
	hEfficy_ = new TH2F("Efficiency"   , "Efficiency plot", 52, 0, 52, 80, 0, 80);

	TDirectory * ParallelDir       = DQMHistosBase::theFile_->mkdir("ParallelDir", "ParallelDir");// <---
	ParallelDir->cd();
	hGaussB_ = new TH1F("GaussianMainB", "Gaussian mainB", 100, 0, 100);
//        theCanvas_ = new TCanvas("canvas","canvas", 800,800) ;
//        theCanvas_->Divide(2,1) ;
        
        TDirectory * ParallelSubDir    = ParallelDir->mkdir("ParallelSubDir", "ParallelSubDir");      // <---
	ParallelSubDir->cd();
	hGaussC_ = new TH1F("GaussianParallelSubDirC", "Gaussian ParallelSubC", 100, 0, 100);

        TDirectory * ParallelSubSubDir = ParallelSubDir->mkdir("ParallelSubSubDir", "MainSubSubDir"); // <---
	ParallelSubSubDir->cd();
	hGaussD_ = new TH1F("GaussianParallelSubSubDirD", "Gaussian ParallelSubSubDirD", 100, 0, 100);

	std::cout << __PRETTY_FUNCTION__ << "Starting!" << std::endl;
	DataConsumer::startProcessingData(runNumber);
	std::cout << __PRETTY_FUNCTION__ << "Started!" << std::endl;
}

//========================================================================================================================
void DQMHistoMakerConsumer::stopProcessingData(void)
{
        saveFile_ = true ;
	DataConsumer::stopProcessingData();
	if(saveFile_)
	{
                STDLINE("Saving all!!!!!!",ACYellow) ; 
		DQMHistosBase::save();
	}
	closeFile();
}

//========================================================================================================================
void DQMHistoMakerConsumer::pauseProcessingData(void)
{
	DataConsumer::stopProcessingData();
}

//========================================================================================================================
void DQMHistoMakerConsumer::resumeProcessingData(void)
{
	DataConsumer::startProcessingData("");
}

//========================================================================================================================
bool DQMHistoMakerConsumer::workLoopThread(toolbox::task::WorkLoop* workLoop)
{
	//std::cout << __COUT_HDR_FL__ << __PRETTY_FUNCTION__ << DataProcessor::processorUID_ << " running, because workloop: " << WorkLoop::continueWorkLoop_ << std::endl;
	fastRead();
	return WorkLoop::continueWorkLoop_;
}

//========================================================================================================================
void DQMHistoMakerConsumer::fastRead(void)
{
	if(DataConsumer::read(dataP_, headerP_) < 0)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(5));
		return;
	}
	double value = 0;

	memcpy(&value, &dataP_->at(0), sizeof(double));
	std::cout << __LINE__ << "] [" << __PRETTY_FUNCTION__ << "] Value: " << value << std::endl;

        for(int bx=1; bx<=53; bx++)
        {
         for(int by=1; by<=80; by++)
         {
          hEfficy_->SetBinContent(bx,by,random_->Gaus(99.6, 0.2)) ;
         }
        }

        for(int i=0; i<random_->Gaus(10,4); ++i)
        {
	 hGaussA_->Fill(random_->Gaus(50,7));
	 hGaussB_->Fill(random_->Gaus(28,7));
	 hGaussC_->Fill(random_->Gaus(61,12));
        }
	DataConsumer::setReadSubBuffer<std::string, std::map<std::string, std::string>>();
}

DEFINE_OTS_PROCESSOR(DQMHistoMakerConsumer)
