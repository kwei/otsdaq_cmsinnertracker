#include "otsdaq-cmsinnertracker/DataProcessorPlugins/DQMMiddlewareHistosConsumer.h"
#include "otsdaq/MessageFacility/MessageFacility.h"
#include "otsdaq/Macros/CoutMacros.h"
#include "otsdaq/Macros/ProcessorPluginMacros.h"
#include "otsdaq-cmsinnertracker/Ph2_ACF/Utils/easylogging++.h"

#include "otsdaq-cmsinnertracker/Ph2_ACF/Utils/ObjectStream.h"
#include "otsdaq-cmsinnertracker/Ph2_ACF/Utils/Container.h"
#include "otsdaq-cmsinnertracker/Ph2_ACF/DQMUtils/DQMHistogramPedeNoise.h"
#include "otsdaq-cmsinnertracker/Ph2_ACF/DQMUtils/DQMHistogramPedestalEqualization.h"
#include "otsdaq-cmsinnertracker/Ph2_ACF/DQMUtils/DQMHistogramCalibrationExample.h"
#include "otsdaq-cmsinnertracker/Ph2_ACF/DQMUtils/RD53PixelAliveHistograms.h"
#include "otsdaq-cmsinnertracker/Ph2_ACF/DQMUtils/RD53PhysicsHistograms.h"
//#include "otsdaq-cmsinnertracker/Ph2_ACF/DQMUtils/SSAPhysicsHistograms.h"
#include "otsdaq-cmsinnertracker/Ph2_ACF/System/FileParser.h"

//#include "otsdaq-cmsinnertracker/Ph2_ACF/DQMUtils/DQMEvent.cc"
#include <TDirectory.h>
#include <TFile.h>
//#include <TROOT.h>

#include <unistd.h>

INITIALIZE_EASYLOGGINGPP

using namespace ots;

//========================================================================================================================
DQMMiddlewareHistosConsumer::DQMMiddlewareHistosConsumer(std::string supervisorApplicationUID, std::string bufferUID, std::string processorUID, const ConfigurationTree &theXDAQContextConfigTree, const std::string &configurationPath)
	: WorkLoop(processorUID), DQMHistosConsumerBase(supervisorApplicationUID, bufferUID, processorUID, LowConsumerPriority), Configurable(theXDAQContextConfigTree, configurationPath), saveFile_(theXDAQContextConfigTree.getNode(configurationPath).getNode("SaveFile").getValue<bool>()), filePath_(theXDAQContextConfigTree.getNode(configurationPath).getNode("FilePath").getValue<std::string>()), radixFileName_(theXDAQContextConfigTree.getNode(configurationPath).getNode("RadixFileName").getValue<std::string>())
	, calibrationName_  (theXDAQContextConfigTree_.getNode(configurationPath).getNode("CalibrationName").getValue<std::string>())
	, configurationDir_ (theXDAQContextConfigTree_.getNode(configurationPath).getNode("ConfigurationDir").getValue<std::string>())
	, configurationName_(theXDAQContextConfigTree_.getNode(configurationPath).getNode("ConfigurationName").getValue<std::string>())
//, dqmh_                (0)

{
	//	gStyle->SetPalette(1);
}

//========================================================================================================================
DQMMiddlewareHistosConsumer::~DQMMiddlewareHistosConsumer(void)
{
	//DQMHistosBase::closeFile();
}
//========================================================================================================================
void DQMMiddlewareHistosConsumer::startProcessingData(std::string runNumber)
{
	const std::string configurationFilePath = configurationDir_ + "/" + configurationName_;
	Ph2_System::FileParser fParser;
	std::map<uint16_t, Ph2_HwInterface::BeBoardFWInterface *> fBeBoardFWMap;
	std::vector<Ph2_HwDescription::BeBoard *> fBoardVector;
	std::stringstream out;
	DetectorContainer fDetectorStructure;
	std::unordered_map<std::string, double> pSettingsMap;

	try
	{
		fParser.parseHW(configurationFilePath, fBeBoardFWMap, fBoardVector, &fDetectorStructure, out, true);
	}
	catch(const std::exception& e)
	{
		__COUT__ << "Error: " << e.what();
		throw std::runtime_error(e.what());
		return;
	}

	fParser.parseSettings(configurationFilePath, pSettingsMap, out, true);

	DQMHistosBase::openFile(filePath_ + "/otsdaq_" + runNumber + ".root");
	//if calibration type pedenoise
	if (calibrationName_ == "pedenoise")
	{
		fDQMHistogrammerVector.push_back(new DQMHistogramPedeNoise());
	}
	else if (calibrationName_ == "calibrationandpedenoise")
	{
		fDQMHistogrammerVector.push_back(new DQMHistogramPedestalEqualization());
		fDQMHistogrammerVector.push_back(new DQMHistogramPedeNoise());
	}
	else if (calibrationName_ == "calibrationexample")
	{
		fDQMHistogrammerVector.push_back(new DQMHistogramCalibrationExample());
	}
	else if (calibrationName_ == "pixelalive")
	{
		fDQMHistogrammerVector.push_back(new PixelAliveHistograms());
	}
	else if (calibrationName_ == "physics")
		fDQMHistogrammerVector.push_back(new PhysicsHistograms());
	// else if (calibrationName_ == "ssaphysics")
	// 	fDQMHistogrammerVector.push_back(new SSAPhysicsHistograms());

	std::cout << "7" << std::endl;
	//DQMHistosBase::myDirectory_ = DQMHistosBase::theFile_->mkdir("OuterTracker", "OuterTracker");
	//DQMHistosBase::myDirectory_->cd();
	for (auto dqmHistogrammer : fDQMHistogrammerVector)
		dqmHistogrammer->book(theFile_, fDetectorStructure, pSettingsMap);

	fPacketNumber = -1;
	fDataBuffer.clear();

	DataConsumer::startProcessingData(runNumber);
	std::cout << __PRETTY_FUNCTION__ << ": Done starting DQM!" << std::endl;
}

//========================================================================================================================
void DQMMiddlewareHistosConsumer::stopProcessingData(void)
{
	if (fDataBuffer.size() > 0)
	{
		std::cout << __PRETTY_FUNCTION__ << " Buffer should be empty, some data were not read, Aborting " << std::endl;
		abort();
	}

	{
	  std::unique_lock<std::mutex> lock(DQMHistosConsumerBase::fillHistoMutex_);
	  for (auto dqmHistogrammer : fDQMHistogrammerVector)
	    dqmHistogrammer->process();
	}

	DataConsumer::stopProcessingData();
	if (saveFile_)
	{
		DQMHistosBase::save();
	}
	closeFile();
	for (auto dqmHistogrammer : fDQMHistogrammerVector)
		delete dqmHistogrammer;
	fDQMHistogrammerVector.clear();

}

//========================================================================================================================
void DQMMiddlewareHistosConsumer::pauseProcessingData(void)
{
	DataConsumer::stopProcessingData();
}

//========================================================================================================================
void DQMMiddlewareHistosConsumer::resumeProcessingData(void)
{
	DataConsumer::startProcessingData("");
}

//========================================================================================================================
bool DQMMiddlewareHistosConsumer::workLoopThread(toolbox::task::WorkLoop *workLoop)
{
	//	std::cout << __PRETTY_FUNCTION__ << DataProcessor::processorUID_ << " running, because workloop: " << WorkLoop::continueWorkLoop_ << std::endl;
	fastRead();
	return WorkLoop::continueWorkLoop_;
}

//========================================================================================================================
void DQMMiddlewareHistosConsumer::fastRead(void)
{
	//std::cout << __PRETTY_FUNCTION__ << "TRYING TO GET DATA!" << std::endl;
	if (DataConsumer::read(dataP_, headerP_) < 0)
	{
		usleep(100);
		return;
	}
	// std::cout << __PRETTY_FUNCTION__ << "GOT DATA AND Start Processing buffer block." << std::endl;
	// std::cout << __PRETTY_FUNCTION__ << "GOT DATA AND Start Processing buffer block." << std::endl;
	// std::cout << __PRETTY_FUNCTION__ << "GOT DATA AND Start Processing buffer block." << std::endl;

	CheckStream *theCurrentStream;
	// std::cout << __PRETTY_FUNCTION__ << "Got Something" << std::endl;
	fDataBuffer.insert(fDataBuffer.end(), dataP_->begin(), dataP_->end());
	while (fDataBuffer.size() > 0)
	{
		// std::cout << "fDataBuffer size: " << fDataBuffer.size() << std::endl;
		if (fDataBuffer.size() < sizeof(CheckStream))
		{
			// std::cout << __PRETTY_FUNCTION__ << " " << __LINE__ << std::endl;
			break; // Not enough bytes to retreive the packet size
		}
		theCurrentStream = reinterpret_cast<CheckStream *>(&fDataBuffer.at(0));
		// std::cout << __PRETTY_FUNCTION__ << " Packet Number received " << int(theCurrentStream->getPacketNumber()) << std::endl;
		if (fPacketNumber < 0)
			fPacketNumber = int(theCurrentStream->getPacketNumber()); // first packet received
		else if (theCurrentStream->getPacketNumber() != fPacketNumber)
		{
			std::cout << __PRETTY_FUNCTION__ << " Packet Number expected " << --fPacketNumber << " But received "
					  << int(theCurrentStream->getPacketNumber()) << ", Aborting" << std::endl;
			// std::cout << __PRETTY_FUNCTION__ << " Did you check that the Endianness of the two comupters is the same???" << std::endl;
			abort();
		}
		// std::cout << __PRETTY_FUNCTION__ << " vector size " << fDataBuffer.size() << " expected " << theCurrentStream->getPacketSize() << std::endl;
		if (fDataBuffer.size() < theCurrentStream->getPacketSize())
		{
			// std::cout << __PRETTY_FUNCTION__ << " " << __LINE__ << std::endl;
			break; // Packet not completed, waiting
		}
		std::vector<char> streamDataBuffer(fDataBuffer.begin(), fDataBuffer.begin() + theCurrentStream->getPacketSize());
		fDataBuffer.erase(fDataBuffer.begin(), fDataBuffer.begin() + theCurrentStream->getPacketSize());
		for (auto dqmHistogrammer : fDQMHistogrammerVector)
		{
			if (dqmHistogrammer->fill(streamDataBuffer))
			{
				if(fPacketNumber==0)
				{
					std::unique_lock<std::mutex> lock(DQMHistosConsumerBase::fillHistoMutex_);
					dqmHistogrammer->process();
				}
			}
		}
		if (++fPacketNumber >= 256)
			fPacketNumber = 0;
	}

	DataConsumer::setReadSubBuffer<std::string, std::map<std::string, std::string>>();
	// __MOUT__ << __PRETTY_FUNCTION__ << ": Done Processing buffer block." << std::endl;
}

DEFINE_OTS_PROCESSOR(DQMMiddlewareHistosConsumer)
