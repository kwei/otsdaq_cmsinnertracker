#ifndef _ots_DQMHistoMakerConsumer_h_
#define _ots_DQMHistoMakerConsumer_h_

#include "otsdaq/DataManager/DQMHistosConsumerBase.h"
#include "otsdaq/Configurable/Configurable.h"
#include <string>
#include "TH2F.h"

class TFile     ;
class TDirectory;
class TH1F      ;
class TH2F      ;
class TRandom   ;
class TCanvas   ;

namespace ots
{

class DQMHistoMakerConsumer : public DQMHistosConsumerBase, public Configurable
{
public:
  DQMHistoMakerConsumer(std::string supervisorApplicationUID, std::string bufferUID, std::string processorUID, const ConfigurationTree& theXDAQContextConfigTree, const std::string& configurationPath);
	virtual ~DQMHistoMakerConsumer(void);

	void startProcessingData (std::string               runNumber) override;
	void stopProcessingData  (void                               ) override;
	void pauseProcessingData (void                               ) override;
	void resumeProcessingData(void                               ) override;
	void load                (std::string               fileName ){;}

private:
	bool workLoopThread      (toolbox::task::WorkLoop * workLoop )         ;
	void fastRead            (void                               )         ;
	void slowRead            (void                               )         ;
	
	//For fast read
	std::string*                       dataP_        ;
	std::map<std::string,std::string>* headerP_      ;

	bool                               saveFile_     ; //yes or no
        TRandom                          * random_       ;
	std::string                        filePath_     ;
	std::string                        radixFileName_;

        TCanvas                          * theCanvas_    ;
	TH1F                             * hGaussA_      ;
	TH1F                             * hGaussB_      ;
	TH1F                             * hGaussC_      ;
	TH1F                             * hGaussD_      ;
	TH2F                             * hEfficy_      ;
};
}

#endif
