#include "otsdaq-cmsoutertracker/DataProcessorPlugins/OTDQMHistosConsumer.h"
#include "otsdaq/MessageFacility/MessageFacility.h"
#include "otsdaq/Macros/CoutMacros.h"
#include "otsdaq/Macros/ProcessorPluginMacros.h"
//#include "otsdaq-cmsoutertracker/OTRootUtilities/SLinkDQMHistogrammer.h"
#include "otsdaq-cmsoutertracker/Ph2_ACF/DQMUtils/SLinkDQMHistogrammer.h"
#include "otsdaq-cmsoutertracker/Ph2_ACF/Utils/SLinkEvent.h"
#include "otsdaq-cmsoutertracker/Ph2_ACF/DQMUtils/DQMEvent.cc"
#include <TDirectory.h>
#include <TFile.h>
//#include <TROOT.h>


#include <unistd.h>

INITIALIZE_EASYLOGGINGPP

using namespace ots;

//========================================================================================================================
OTDQMHistosConsumer::OTDQMHistosConsumer(std::string supervisorApplicationUID, std::string bufferUID, std::string processorUID, const ConfigurationTree& theXDAQContextConfigTree, const std::string& configurationPath)
: WorkLoop             (processorUID)
, DQMHistosConsumerBase(supervisorApplicationUID, bufferUID, processorUID, LowConsumerPriority)
, Configurable         (theXDAQContextConfigTree, configurationPath)
, saveFile_            (theXDAQContextConfigTree.getNode(configurationPath).getNode("SaveFile").getValue<bool>())
, filePath_            (theXDAQContextConfigTree.getNode(configurationPath).getNode("FilePath").getValue<std::string>())
, radixFileName_       (theXDAQContextConfigTree.getNode(configurationPath).getNode("RadixFileName").getValue<std::string>())
, dqmh_                (0)

{
	__MOUT__ << __PRETTY_FUNCTION__ << ": OT CONSUMER Constructor!" << std::endl;
	//	gStyle->SetPalette(1);
}

//========================================================================================================================
OTDQMHistosConsumer::~OTDQMHistosConsumer(void)
{
	DQMHistosBase::closeFile();
}
//========================================================================================================================
void OTDQMHistosConsumer::startProcessingData(std::string runNumber)
{

	//std::cout << __PRETTY_FUNCTION__ << "using this file: " << cHWFile_ << std::endl;
	//cSystemController.InitializeHw( cHWFile_ );
	//cSystemController.ConfigureHw ( std::cout );

	//bool addTree   = false;
	//int ncol       = 1;
	//bool evtFilter = false;
	//bool skipHist  = false;
	//if(dqmh_ != 0) delete dqmh_;
	//
	//dqmh_ = new DQMHistogrammer (addTree, ncol, evtFilter, skipHist);

	DQMHistosBase::openFile(filePath_ + "/otsdaq_" + runNumber + ".root");
	DQMHistosBase::myDirectory_ = DQMHistosBase::theFile_->mkdir("OuterTracker", "OuterTracker");
	DQMHistosBase::myDirectory_->cd();



	//  // //GLIB BOARD I GUESS
	//  // cBoardTypeString = "GLIB";
	//  // //this is legacy as the fNCbcDataSize is not used any more
	//  // //cNEventSize32 = (cBoard->getNCbcDataSize() == 0 ) ? EVENT_HEADER_TDC_SIZE_32 + cNCbc * CBC_EVENT_SIZE_32 : EVENT_HEADER_TDC_SIZE_32 + cBoard->getNCbcDataSize() * CBC_EVENT_SIZE_32;
	//  // if (cNCbc <= 4)
	//  // 	cNEventSize32 = EVENT_HEADER_TDC_SIZE_32 + 4 * CBC_EVENT_SIZE_32;
	//  // else if (cNCbc > 4 && cNCbc <= 8)
	//  // 	cNEventSize32 = EVENT_HEADER_TDC_SIZE_32 + 8 * CBC_EVENT_SIZE_32;
	//  // else if (cNCbc > 8 && cNCbc <= 16)
	//  // 	cNEventSize32 = EVENT_HEADER_SIZE_32 + 16 * CBC_EVENT_SIZE_32;



	//// Get number of front end boards and number of CBC's
	//uint8_t cNFe = cSystemController.getBoard ( 0 )->getNFe();
	//uint16_t cNCbc = 0;//pBoard->getNCbc() / cNFe;
	//for (const auto& cFe : cSystemController.getBoard ( 0 )->fModuleVector)
	//  cNCbc += cFe->getNCbc();

	//// Create dummy event map, needed for the creation of the histograms
	//// In the offline DQM, this event map is taken from the first event,
	//// here I create a dummy event (0) to achieve the same
	//std::vector<uint32_t> list{uint32_t(0)};
	//std::map<uint16_t, std::vector<uint32_t>> dummyEventDataMap;
	//eventDataSize_ = D19C_EVENT_HEADER1_SIZE_32_CBC3 + cNFe * D19C_EVENT_HEADER2_SIZE_32_CBC3 + cNCbc * CBC_EVENT_SIZE_32_CBC3;

	//// Fill the event map with the dummy event
	//for ( uint8_t cFeId = 0; cFeId < cNFe; cFeId++ ){
	//    for ( uint8_t cCbcId = 0; cCbcId < cNCbc; cCbcId++ ){
	//        uint16_t cKey = cFeId << 8 | cCbcId;

	//    // GLIB CASE
	//        //uint32_t begin = EVENT_HEADER_SIZE_32 + cFeId * CBC_EVENT_SIZE_32 * cNCbc + cCbcId * CBC_EVENT_SIZE_32;
	//        //uint32_t end = begin + CBC_EVENT_SIZE_32 - 1;
	//        uint32_t begin = D19C_EVENT_HEADER1_SIZE_32_CBC3 + cFeId * D19C_EVENT_HEADER2_SIZE_32_CBC3 + cCbcId * CBC_EVENT_SIZE_32_CBC3;
	//        uint32_t end = begin + CBC_EVENT_SIZE_32_CBC3 - 1;

	//        std::vector<uint32_t> cCbcData (std::next (std::begin (list), begin), std::next (std::begin (list), end) );

	//        dummyEventDataMap[cKey] = cCbcData;
	//    }
	//}
	//
	// Book histos by passing the dummy event map
	//dqmh_->bookHistograms (dummyEventDataMap);

	// Read SLink event data from CBC3
	// Create DQM object
	__MOUT__ << __PRETTY_FUNCTION__ << ": Starting SLinkDQMHistogrammer" << std::endl;
	dqmh_ = new SLinkDQMHistogrammer(0);

	// Filename with dummy event to book the histograms
	// This is not generic, because depending on the setup, you need a different
	// DAQ file
	const std::string& filename = std::string(getenv("OTSDAQ_CMSOUTERTRACKER_DIR")) + "/otsdaq-cmsoutertracker/OTRootUtilities/dummy_event_cbc3.daq";

	// Vector with events (only one in this case)
	std::vector<DQMEvent*> evList;

	// What follows is mainly a copy from miniDAQ/miniSLinkDQM.cc:readSLinkFromFile()
	// Open binary file with dummy event
	__MOUT__ << __PRETTY_FUNCTION__ << ": Open file to read dummy event from " << filename << std::endl;
	std::ifstream fh(filename, std::fstream::in | std::fstream::binary);
	if (!fh){
		__MOUT__ << __PRETTY_FUNCTION__ << ": Error opening file " << filename << "!" << std::endl;
		abort();
	}
	__MOUT__ << __PRETTY_FUNCTION__ << ": File opened successfully." << std::endl;

	std::vector<uint64_t> cData;
	bool trailerFound = false;

	__MOUT__ << __PRETTY_FUNCTION__ << ": Look for trailer in dummy data to identify setup." << std::endl;
	while(!fh.eof()){
		char buffer[8];
		fh.read(buffer, sizeof (uint64_t));

		if (!fh){
			__MOUT__ << __PRETTY_FUNCTION__ << ": Error reading dummy data from file " << filename << "!" << std::endl;
			abort();
		}

		uint64_t word;
		//fh.read((char*) &word, sizeof (uint64_t));
		std::memcpy(&word, buffer, sizeof (uint64_t));
		uint64_t correctedWord = (word & 0xFFFFFFFF) << 32 | ((word >> 32) & 0xFFFFFFFF);
		cData.push_back (correctedWord);

		// Now find the last word of the event
		if ((correctedWord & 0xFF00000000000000) >> 56 == 0xA0 &&
				(correctedWord & 0x00000000000000F0) >> 4  == 0x7){   // SLink Trailer
			trailerFound = true;
			break;
		}
	}

	// 3 is the minimum size for an empty SLinkEvent (2 words header and 1 word trailer)
	if (trailerFound && cData.size() > 3){
		DQMEvent* ev = new DQMEvent(new SLinkEvent(cData));
		evList.push_back(ev);
		__MOUT__ << __PRETTY_FUNCTION__ << ": Found " << evList.size() << " events in dummy data." << std::endl;
		dqmh_->bookHistograms(ev->trkPayload().feReadoutMapping());
	}else{
		__MOUT__ << __PRETTY_FUNCTION__ << ": Could not find proper SLinkEvent in dummy data." << std::endl;
		abort();
	}

	DataConsumer::startProcessingData(runNumber);
	__MOUT__ << __PRETTY_FUNCTION__ << ": Done starting DQM!" << std::endl;
}

//========================================================================================================================
void OTDQMHistosConsumer::stopProcessingData(void)
{
	DataConsumer::stopProcessingData();
	if(saveFile_)
	{
		DQMHistosBase::save();
	}
	closeFile();
}

//========================================================================================================================
void OTDQMHistosConsumer::pauseProcessingData(void)
{
	DataConsumer::stopProcessingData();
}

//========================================================================================================================
void OTDQMHistosConsumer::resumeProcessingData(void)
{
	DataConsumer::startProcessingData("");
}

//========================================================================================================================
bool OTDQMHistosConsumer::workLoopThread(toolbox::task::WorkLoop* workLoop)
{
	//std::cout << __COUT_HDR_FL__ << __PRETTY_FUNCTION__ << DataProcessor::processorUID_ << " running, because workloop: " << WorkLoop::continueWorkLoop_ << std::endl;
	fastRead();
	return WorkLoop::continueWorkLoop_;
}

//========================================================================================================================
void OTDQMHistosConsumer::fastRead(void)
{
	if(DataConsumer::read(dataP_, headerP_) < 0)
	{
		usleep(100);
		return;
	}
	__MOUT__ << __PRETTY_FUNCTION__ << ": Start Processing buffer block." << std::endl;
	cData_.clear();
	cData_.resize(dataP_->length()/sizeof(uint64_t));
	for(unsigned int l=0; l<dataP_->length(); l+=sizeof(uint64_t))
	{
		std::memcpy(&cData_[l/sizeof(uint64_t)], &((*dataP_)[l]), sizeof(uint64_t));
	}

	__MOUT__ << __PRETTY_FUNCTION__ << ": " << processorUID_ << " Got data!" << " datap: " << dataP_->length() << " cdata: " << cData_.size() << " ev size: " << eventDataSize_ << std::endl;

	// std::cout << __PRETTY_FUNCTION__ << ">>>Consumer Recorded Events # " << cData.size() << std::endl;
	// for(unsigned int i=0; i<cData.size(); i++)
	// 	std::cout << __PRETTY_FUNCTION__ << "# "<< i << " val: " << cData[i] << std::endl;

	//__MOUT__ << __PRETTY_FUNCTION__ << ": Read data from SystemController." << std::endl;
	//BeBoard* pBoard = cSystemController.fBoardVector.at(0);
	//const std::vector<Event*>& events = cSystemController.GetEvents(pBoard);
	//uint32_t cPacketSize = cSystemController.ReadData(pBoard);
	//std::vector<DQMEvent*> cDQMEvents;

	//__MOUT__ << __PRETTY_FUNCTION__ << ": Loop over data." << std::endl;
	//for (auto& ev : events){
	//	SLinkEvent cSLev = ev->GetSLinkEvent(pBoard);
	//	cDQMEvents.emplace_back (new DQMEvent (&cSLev) );
	//	dqmh_->fillHistograms(cDQMEvents);
	//	cDQMEvents.clear();
	//}
	//__MOUT__ << __PRETTY_FUNCTION__ << ": Done reading data." << std::endl;

    // Vector with one event only
    //DQMEvent cDQMEvent(cData_);
    std::vector<DQMEvent*> cDQMEvents;
    cDQMEvents.emplace_back(new DQMEvent(cData_));
    dqmh_->fillHistograms(cDQMEvents);

	//dataHelper_.Reset();
	//dataHelper_.Set ( cSystemController.getBoard ( 0 ), cData_, cData_.size()/eventDataSize_, cSystemController.fBeBoardInterface->getBoardType (cSystemController.getBoard(0)) );
	//eventList_ = dataHelper_.GetEvents ( cSystemController.getBoard ( 0 ) );
	//std::cout << __PRETTY_FUNCTION__ << "cData " << cData.size() << " nEvents " << nEvents << " event " << elist.size() << std::endl;
	// uint32_t count = 0;
	// for ( auto& ev : elist )
	// {
	// 	std::cout << __PRETTY_FUNCTION__<< "Event #:" << count++ << std::endl;
	// 	std::cout << *ev << std::endl;
	// 	std::cout << "COMPARING@@@@@@@@@@@@@@@@@@@@@@@@@"<<std::endl;
	// }
	//std::cout << __COUT_HDR_FL__ << __PRETTY_FUNCTION__ << processorUID_ << " Buffer: " << dataP_->size() << std::endl;
	//dqmh_->fillHistograms(eventList_);
	//std::cout << __PRETTY_FUNCTION__ << processorUID_ << " filled histos!" << std::endl;

	DataConsumer::setReadSubBuffer<std::string, std::map<std::string, std::string>>();
	// __MOUT__ << __PRETTY_FUNCTION__ << ": Done Processing buffer block." << std::endl;
}

DEFINE_OTS_PROCESSOR(OTDQMHistosConsumer)
