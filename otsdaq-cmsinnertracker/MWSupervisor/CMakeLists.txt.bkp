if(NOT DEFINED ENV{OTSDAQ_CMSOUTERTRACKER_DIR})
cmake_minimum_required(VERSION 2.8)
project(MWSupervisor)

# set the output directory
if ( ${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR} )
    message( FATAL_ERROR "In-source builds not allowed. Please make a new directory (called a build directory) and run CMake from there. You may need to remove CMakeCache.txt." )
endif()

# ---------- Setup output Directories -------------------------
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY
    ${PROJECT_SOURCE_DIR}/lib/${XDAQ_OS}/${XDAQ_PLATFORM}
    CACHE PATH
   "Single Directory for all Libraries")

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY
   ${PROJECT_SOURCE_DIR}/bin
   CACHE PATH
   "Single Directory for all Executables."
   )

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY
   ${PROJECT_SOURCE_DIR}/bin
   CACHE PATH
   "Single Directory for all static libraries."
   )
# ---------- Setup output Directories -------------------------

#set the cmakeL module path
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/")

# compiler flags
set (CMAKE_CXX_FLAGS
    "-std=c++11 -O3 -Wcpp -pthread -pedantic -Wall -w -g -fPIC -Wno-deprecated-declarations
	$(shell pkg-config --cflags libxml++-2.6)
	$(shell xslt-config --cflags)
	$(shell root-config --cflags)
    ${CMAKE_CXX_FLAGS}")

#check for external dependences
message("#### Checking for external Dependencies ####")
#ROOT
find_package(ROOT COMPONENTS RHTTP)
if(ROOT_FOUND)
    #check for THttpServer
    if(EXISTS ${ROOT_RHTTP_LIBRARY})
        message(STATUS "Found THttp Server support - enabling compiler flags")
        set(ROOT_HAS_HTTP TRUE)
    elseif()
        message(STATUS "ROOT built without THttp Server - disabling compiler flags")
    endif()
    message(STATUS "Using ROOT version ${ROOT_VERSION}")
endif(ROOT_FOUND)

#ZeroMQ optional
find_package(ZMQ)
if(ZMQ_FOUND)
    #message(STATUS "Found ZMQ Library - checking for Ph2_UsbInstLib")

    #libPh2_UsbInstLib
    find_package(PH2_USBINSTLIB)
    if(PH2_USBINSTLIB_FOUND)
        #message(STATUS "Found Ph2_UsbInstLib - enabling support")
    endif(PH2_USBINSTLIB_FOUND)
endif(ZMQ_FOUND)

#Boost
file(GLOB_RECURSE uhal_boost /opt/cactus/*version.hpp)
if(uhal_boost)
    MESSAGE(STATUS "Found boost installation that comes with uHAL, using this version")
        set(Boost_NO_SYSTEM_PATHS TRUE)
        if(Boost_NO_SYSTEM_PATHS)
            if(${CACTUS_FOUND})
                set(BOOST_ROOT ${CACTUS_ROOT})
            else(${CACTUS_FOUND})
                set(BOOST_ROOT /opt/cactus)
            endif(${CACTUS_FOUND})
            set(BOOST_INCLUDE_DIRS ${BOOST_ROOT}/include)
            set(BOOST_LIBRARY_DIRS ${BOOST_ROOT}/lib)
        endif(Boost_NO_SYSTEM_PATHS)
else(uhal_boost)
    MESSAGE(STATUS "No boost headers found with uHAL (you are most likely using CC7) - make sure to install boost 1.53 (default version on CC7)")
endif(uhal_boost)

find_package(Boost 1.53 REQUIRED system filesystem thread program_options)

add_subdirectory(src)