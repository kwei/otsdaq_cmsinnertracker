#ifndef __MWSupervisor_MWMockup_H__
#define __MWSupervisor_MWMockup_H__

#include "xdaq/Application.h"
#include "xdaq/NamespaceURI.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"
#include "xoap/MessageReference.h"

#include "cgicc/HTMLClasses.h"

namespace Ph2TkDAQ {

  class MWMockup : public xdaq::Application,
                   public xgi::framework::UIManager
  {
    public:
      XDAQ_INSTANTIATOR();

      MWMockup(xdaq::ApplicationStub* s) throw (xdaq::exception::Exception);
      void Default(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);

      xoap::MessageReference sayHello(xoap::MessageReference msg) throw (xoap::exception::Exception);
  };

}

#endif