#ifndef __MWUtils_FilePaths_H__
#define __MWUtils_FilePaths_H__

#define HOME "${MWSUPERVISOR_ROOT}"
#define XSLSTYLESHEET "${MWSUPERVISOR_ROOT}/xml/XMLtoHTML.xsl"
#define XMLSTYLESHEET "${MWSUPERVISOR_ROOT}/xml/HTMLtoXML.xsl"
#define SETTINGSSTYLESHEET "${MWSUPERVISOR_ROOT}/xml/SettingsXMLtoHTML.xsl"
#define SETTINGSSTYLESHEETINVERSE "${MWSUPERVISOR_ROOT}/xml/SettingsHTMLtoXML.xsl"
#define CSSSTYLESHEET "${MWSUPERVISOR_ROOT}/html/Stylesheet.css"

#endif