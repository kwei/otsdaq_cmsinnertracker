/* Copyright 2017 Imperial Collge London
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   Programmer :     Georg Auzinger
   Version :        1.0
   Date of creation:01/09/2017
   Support :        mail to : georg.auzinger@SPAMNOT.cern.ch
   FileName :       version.h
*/

#ifndef __MWSupervisor_version_H__
#define __MWSupervisor_version_H__

#include "config/PackageInfo.h"

#define MWSUPERVISOR_VERSION_MAJOR 1
#define MWSUPERVISOR_VERSION_MINOR 0
#define MWSUPERVISOR_VERSION_PATCH 0
#undef MWSUPERVISOR_PREVIOUS_VERSIONS

#define MWSUPERVISOR_VERSION_CODE PACKAGE_VERSION_CODE( \
  MWSUPERVISOR_VERSION_MAJOR, \
  MWSUPERVISOR_VERSION_MINOR, \
  MWSUPERVISOR_VERSION_PATCH \
)
#ifndef MWSUPERVISOR_PREVIOUS_VERSIONS
#define MWSUPERVISOR_FULL_VERSION_LIST PACKAGE_VERSION_STRING( \
  MWSUPERVISOR_VERSION_MAJOR, \
  MWSUPERVISOR_VERSION_MINOR, \
  MWSUPERVISOR_VERSION_PATCH \
)
#else
#define MWSUPERVISOR_FULL_VERSION_LIST MWSUPERVISOR_PREVIOUS_VERSIONS ","
PACKAGE_VERSION_STRING ( \
  MWSUPERVISOR_VERSION_MAJOR, \
  MWSUPERVISOR_VERSION_MINOR, \
  MWSUPERVISOR_VERSION_PATCH \
)
#endif

namespace MWSupervisor {
    const std::string package = "MWSupervisor";
    const std::string versions = MWSUPERVISOR_FULL_VERSION_LIST;
    const std::string summary = "uTCA MWSupervisor for Ph2 Tk DAQ";
    const std::string description = "uTCA MWSupervisor for Ph2 Tk DAQ";
    const std::string authors = "Georg Auzinger, Weinan Si";
    const std::string link = "http://xdaq.web.cern.ch";
    config::PackageInfo getPackageInfo();
    void checkPackageDependencies() throw (config::PackageInfo::VersionException);
    std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif